package de.niki.Nikis_Server.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {

        Player player = event.getPlayer();

        if(player.getDisplayName().equals("Niki0504") || player.isOp()) {
            event.setFormat(ChatColor.GOLD + "<%1$s>" + ChatColor.DARK_GREEN + ": " + ChatColor.AQUA + "%2$s");
        } else if(player.getDisplayName().equals("Niki0504") && player.isInvisible()) {
            event.setFormat(ChatColor.RED + "<%1$s>" + ChatColor.DARK_GREEN + ": " + ChatColor.RED + "%2$s");
        } else{
            event.setFormat(ChatColor.WHITE + "<%1$s>" + ChatColor.WHITE + ": " + ChatColor.GRAY + "%2$s");
        }

        event.setMessage((player.isOp() ? ChatColor.translateAlternateColorCodes('&', event.getMessage()) : event.getMessage()));

    }

}
