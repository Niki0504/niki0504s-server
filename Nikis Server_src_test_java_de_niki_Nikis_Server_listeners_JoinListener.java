package de.niki.Nikis_Server.listeners;

import de.niki.Nikis_Server.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.sendMessage(Main.getPrefix() + ChatColor.GOLD + "hello and have fun on the Server.");
        event.setJoinMessage(Main.getPrefix() + ChatColor.YELLOW + player.getName() + ChatColor.AQUA + " have joined the server.");
    }

}