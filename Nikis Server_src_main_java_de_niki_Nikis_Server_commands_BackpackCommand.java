package de.niki.Nikis_Server.commands;

import de.niki.Nikis_Server.Main;
import de.niki.Nikis_Server.utils.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.io.IOException;

public class BackpackCommand implements CommandExecutor {

    private Inventory inventory;
    private Object Inventory;

    public BackpackCommand() {

        int slots = 27;

        if(Config.contains("command.backpack.slots")) {
            slots = (int) Config.get("command.backpack.slots");
        } else {
            try {
                Config.set("command.backpack.slots", 27);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.inventory = Bukkit.createInventory(null, slots, ChatColor.GOLD + "Backpack");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player player = (Player)sender;
            player.openInventory((org.bukkit.inventory.Inventory) Inventory);
        } else {
            sender.sendMessage(Main.getPrefix() + ChatColor.RED + "This command can only be executed while being a player.");
        }
        return false;
    }

    public org.bukkit.inventory.Inventory getInventory() {
        return inventory;
    }

    public void setInventory(org.bukkit.inventory.Inventory inventory) {
        this.inventory = inventory;
    }
}
