package de.niki.Nikis_Server;

import de.niki.Nikis_Server.commands.BackpackCommand;
import de.niki.Nikis_Server.commands.DateCommand;
import de.niki.Nikis_Server.listeners.ChatListener;
import de.niki.Nikis_Server.listeners.JoinListener;
import de.niki.Nikis_Server.listeners.QuitListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        
        Bukkit.getLogger().fine("Plugin wird aktiviert");

        listenerRegistration();
        commandRegistration();
    }

    @Override
    public void onDisable(){ Bukkit.getLogger().fine("Plugin wird deaktiviert");
    }

    public static String getPrefix() {
        return ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "Niki's Server" + ChatColor.DARK_GRAY + "] " + ChatColor.GRAY;
    }

    private void listenerRegistration() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new QuitListener(), this);
        pluginManager.registerEvents(new ChatListener(), this);
    }

    private void commandRegistration(){
        getCommand("date").setExecutor(new DateCommand());
        getCommand("backpack").setExecutor(new BackpackCommand());
    }
}